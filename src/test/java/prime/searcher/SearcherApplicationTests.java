package prime.searcher;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebClientOptions;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import java.net.MalformedURLException;

import static org.junit.Assert.*;
import static com.gargoylesoftware.htmlunit.BrowserVersion.CHROME;
import static org.assertj.core.api.Assertions.assertThat;


/**
 * Tests the Prime Searcher Application.
 * @author Kacper Urbaniec
 * @version 2019-04-26
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SearcherApplicationTests {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private ServletRegistrationBean beanPrimeSearcher;

    private PrimeSearcher servlet;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private MockMvc mockMvc;


    @Before
    public void setupBeforeEach() {
        servlet = (PrimeSearcher) beanPrimeSearcher.getServlet();
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    /**
     * Creates a WebClient-Object that is needed to simulate a "GUI-Less browser"
     * for testing various things.
     */
    private static WebClient createWebClient() throws MalformedURLException {
        WebClient webClient = new WebClient(CHROME);
        WebClientOptions options = webClient.getOptions();
        options.setJavaScriptEnabled(true);
        options.setRedirectEnabled(true);
        return webClient;
    }

    /**
     * Adds a bigger timeout for the servlet, so that new primes will be searched after
     * a longer delay. <br>
     * This longer timeout is needed to test the last saved attributes, because with
     * the default timeout of 100ms, test cases will always compare old data.
     */
    public void syncServlet() throws Exception{
        // Change timeout in which the servlet searches for new primes
        servlet.setTimeout(2000);
        // Wait until new one is found, so that the new timeout will come in effect
        long last = servlet.getLastPrime();
        while (last == servlet.getLastPrime()) {
            Thread.sleep(50);
        }
    }


    @Test
    public void test1_IndexView() {
        /* Following code would be used, if we had a normal Controller
           and not a Servlet
            this.mockMvc.perform(get("/primes"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Prime")));*/
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/primes",
                String.class)).contains("Welcome to Prime Searcher!");
    }

    @Test
    public void test2_SearcherView() {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/primes/searcher",
                String.class)).contains("Prime Searcher - Findings", "The last prime discovered was");
    }

    @Test
    public void test3_IndexRedirect() throws Exception {
        // Create WebClient
        WebClient webClient = createWebClient();
        String originalURL = "http://localhost:" + port + "/primes";
        // Visit page to test redirection, save browser history count
        webClient.getPage(originalURL);
        int initHistory = webClient.getCurrentWindow().getHistory().getLength();
        // Wait 6 seconds, so that redirection will be invoked (through JavaScript
        // after 5 seconds)
        Thread.sleep(6000);
        // Get current browser history count and current url
        int laterHistory = webClient.getCurrentWindow().getHistory().getLength();
        String redURL = webClient.getCurrentWindow().getHistory().getUrl(laterHistory-1).toString();
        // Check if redirection has happened
        assertTrue(initHistory < laterHistory);
        assertEquals("http://localhost:" + port + "/primes/searcher", redURL);
    }

    @Test
    public void test4_InitDate() {
        String initDate = servlet.getInitDate().toString();
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/primes/searcher",
                String.class)).contains(initDate);
    }

    @Test
    public void test5_LastDate() throws Exception {
        this.syncServlet();
        String lastDate = servlet.getLastDate().toString();
        /* Alternative approach to syncServlet, less accurate
        // Remove milliseconds, to add some tolerance in timestamp difference
        lastDate = lastDate.substring(0, lastDate.length() - 4);*/
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/primes/searcher",
                String.class)).contains(lastDate);
        // Restore old timeout
        servlet.setTimeout(100);
    }

    @Test
    public void test6_LastPrime() throws Exception {
        this.syncServlet();
        // Get last prime number and compare to Web-Output
        String lastPrime = String.valueOf(servlet.getLastPrime());
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/primes/searcher",
                String.class)).contains(lastPrime);
        // Restore old timeout
        servlet.setTimeout(100);
    }

    @Test
    public void test7_IndexShutdown() {
        this.restTemplate.getForObject("http://localhost:" + port + "/primes/shutdown",
                String.class);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/primes",
                String.class)).contains("Prime Searcher has been stopped");
    }

    @Test
    public void test7_SearcherShutdown() {
        this.restTemplate.getForObject("http://localhost:" + port + "/primes/shutdown",
                String.class);
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/primes/searcher",
                String.class)).contains("Prime Searcher has been stopped");
    }

    @Test
    public void test8_ThreadShutdown() {
        this.restTemplate.getForObject("http://localhost:" + port + "/primes/shutdown",
                String.class);
        assertFalse(servlet.isAlive());
    }



}
