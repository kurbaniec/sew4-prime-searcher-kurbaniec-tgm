package prime.searcher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

/**
 * Starts the Prime Searcher Application.<br>
 * Additionally a bean called <b>beanPrimeSearcher</b> is created, which
 * creates and registers the {@link PrimeSearcher}-Servlet.
 * @author Kacper Urbaniec
 * @version 2019-04-23
 */
@SpringBootApplication
//@ServletComponentScan //Scans for Servlets and starts them
public class SearcherApplication {

    public static void main(String[] args) {
        SpringApplication.run(SearcherApplication.class, args);
    }

    /**
     * Creates a new {@link PrimeSearcher}-Servlet and maps the URLs "/primes" and
     * "/primes/searcher" to it. <br>
     * It is also registered by Spring and loaded.
     * @return ServletRegistration that holds {@link PrimeSearcher}.
     */
    @Bean
    @SuppressWarnings("unchecked")
    public ServletRegistrationBean beanPrimeSearcher() {
        ServletRegistrationBean bean = new ServletRegistrationBean(
                new PrimeSearcher());
        bean.addUrlMappings("/primes", "/primes/searcher");
        bean.setName("PrimeSearcher");
        bean.setLoadOnStartup(1);
        return bean;
    }

}
