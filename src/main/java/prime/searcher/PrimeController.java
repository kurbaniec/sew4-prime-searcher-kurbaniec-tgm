package prime.searcher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


/**
 * Controller for general requests to the application.
 * @author Kacper Urbaniec
 * @version 2019-04-23
 */
@Controller
public class PrimeController {

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    ServletRegistrationBean beanPrimeSearcher;

    /**
     * Gracefully shuts down the application, when "/shutdown" is called.
     */
    @GetMapping("/shutdown")
    public void shutdown() {
        SpringApplication.exit(ctx, () -> 0);
        System.exit(0);
    }

    /**
     * Stops the internal worker thread of the {@link PrimeSearcher}-Servlet.
     */
    @GetMapping("/primes/shutdown")
    public String primesShutdown(Model model) {
        // Alternative to Autowired
        // ServletRegistrationBean beanPrimeSeacher = ctx.getBean("ServletRegistration", ServletRegistrationBean.class);
        PrimeSearcher servlet = (PrimeSearcher) beanPrimeSearcher.getServlet();
        servlet.stop();
        model.addAttribute("terminatedFlag", false);
        return "index";
    }
}
