package prime.searcher;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;


/**
 * Defines a Servlet, that is deployed on "/primes" and searches continually for prime numbers.<br>
 * The latest finding can be found on "/primes/searcher", which "/primes" also redirects automatically to.
 * @author Kacper Urbaniec
 * @version 2019-04-24
 */

// If a ServletRegistrationBean is not needed, Annotations can be used to start the Servlet.
// @WebServlet(name = "PrimeSearcher", urlPatterns = {"/primes", "/primes/searcher"}, loadOnStartup = 1)
public class PrimeSearcher extends HttpServlet implements Runnable {

    private boolean searching = true;
    private long lastPrime;
    private LocalDateTime initDate;
    private LocalDateTime lastDate;
    private ThreadPoolTaskExecutor executor;
    private Logger logger = LoggerFactory.getLogger(PrimeSearcher.class);
    private int timeout = 100;
    // Needed for HTML-Thymeleaf conversion
    private ClassLoaderTemplateResolver resolver;
    private TemplateEngine engine;

    /**
     * Initialize Servlet.
     */
    @Override
    public void init() throws ServletException {
        // Initialize HTML-Thymeleaf Template-Resolver
        thymeleafInit();
        // Define ExecutorService and start looking for prime numbers
        executor = new ThreadPoolTaskExecutor();
        executor.initialize();
        executor.execute(this);
    }

    /**
     * Runner that looks after prime numbers and stores the last output.<br>
     * The method {@link #isPrime(long)} is used to determine prime numbers.
     */
    @Override
    public void run() {
        logger.info("Starting searching for primes");
        initDate = LocalDateTime.now();
        lastPrime = 2;
        long toTest = 3;
        while (searching) {
            if (isPrime(toTest)) {
                lastPrime = toTest;
                lastDate = LocalDateTime.now();
            }
            toTest += 2;
            try {
                Thread.sleep(timeout);
            } catch (Exception ex) {
                logger.warn("PrimeSearcher-Thread Error: " + ex.getMessage());
            }
        }
    }

    /**
     * Listener for GET-request on mapped urls.<br>
     * Returns a welcome page for "/primes".<br>
     * Returns latest findings for "/primes/searcher".<br>
     * If the internal thread is stopped, the pages will output this information.<br>
     * The pages are dynamically created via Thymeleaf and html-templates.
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String reqURL = req.getServletPath();
        logger.info("Requesting " + reqURL);

        WebContext context = new WebContext(req, res, req.getServletContext());
        if (reqURL.equals("/primes")) {
            context.setVariable("recipient", "World");
            context.setVariable("url", "/primes/searcher");
            context.setVariable("terminatedFlag", searching);
            // Tells to process the Variables to the html-template via Thymeleaf
            String result = engine.process("/index.html", context);
            // Output result
            PrintWriter out = res.getWriter();
            if (result != null) {
                out.println(result);
            }
            else out.println("Error");
            logger.info("Returned page will redirect via inline JavaScript to /primes/searcher");
            /*
            If no page is needed for return, the following code can be used instead
            of the inline javascript-solution:
            try {
                Thread.sleep(5000);
            } catch (Exception ex) {ex.printStackTrace();}
            logger.info("Redirecting now to /primes/searcher");
            res.sendRedirect("/primes/searcher");*/
        }
        else if(reqURL.equals("/primes/searcher")) {
            context.setVariable("lastPrime", lastPrime);
            context.setVariable("initDate", initDate);
            context.setVariable("lastDate", lastDate);
            context.setVariable("terminatedFlag", searching);
            String result = engine.process("/searcher.html", context);
            PrintWriter out = res.getWriter();
            if (result != null) {
                out.println(result);
            }
            else out.println("Error");
        }
    }

    /**
     * Stops the internal worker thread that executes {@link #run()}.
     */
    public void stop() {
        logger.info("Stopping Prime Searcher Thread");
        this.searching = false;
        if(executor.getActiveCount() > 0) {
            try {
                Thread.sleep(1000);
                if(executor.getActiveCount() > 0) {
                    executor.setAwaitTerminationSeconds(0);
                    executor.shutdown();
                }
            }
            catch (Exception ex) {
                logger.warn("Prime Searcher Thread close failed - " + ex.getMessage());
            }
        }
    }


    /**
     * Checks if a number is a prime number. <br>
     * Prime number -> Dividable through 1 and itself<br>
     * Code found <a href="https://www.mkyong.com/java/how-to-determine-a-prime-number-in-java/">here</a>.
     * @param number Number that needs to be checked.
     * @return true if prime, else false.
     */
    private boolean isPrime(long number) {
        // When number is dividable through 2 -> no prime
        if (number %2 == 0) return false;
        // Check other divisors
        for (int i = 3; i*i <=number; i+=2) {
            if(number %i == 0)
                return false;
        }
        return true;
    }

    /**
     * Initializes the Thymeleaf template-resolver.
     */
    private void thymeleafInit() {
        resolver = new ClassLoaderTemplateResolver();
        resolver.setTemplateMode(TemplateMode.HTML);
        resolver.setPrefix("templates/");
        resolver.setCacheable(true);
        resolver.setCacheTTLMs(60000L);
        resolver.setCharacterEncoding("utf-8");
        engine = new TemplateEngine();
        engine.setTemplateResolver(resolver);
    }

    public long getLastPrime() {
        return lastPrime;
    }

    public LocalDateTime getInitDate() {
        return initDate;
    }

    public LocalDateTime getLastDate() {
        return lastDate;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    /**
     * Used to check if internal thread is still working.
     * @return true, when still working, else false.
     */
    public boolean isAlive() {
        return (searching && executor.getActiveCount() == 0);
    }
}
