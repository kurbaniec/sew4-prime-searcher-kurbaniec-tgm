# "*Prime Searcher*"

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Implementierung

Mit folgenden Befehl kann die Applikation gestartet werden:

```
gradle clean build test bootRun
```

Vor der eigentlichen Ausführung werden noch die Tests ausgeführt, falls dieser Schritt übersprungen werden soll, muss man nur `test` im Kommando auslassen.

---

Zu aller erst habe ich begonnen am Web-Servlet zu arbeiten. Dazu habe ich die Klasse `PrimeSearcher` definiert und diese nach folgender [Anleitung](<https://www.baeldung.com/context-servlet-initialization-param>) in ein einfaches Servlet gewandelt. Die Konfiguration dieses erfolgte über die Annotation `@WebServlet(name = "PrimeSearcher", urlPatterns = {"/primes", "/primes/searcher"}, loadOnStartup = 1)`.

Ich erstellte als nächstes folgenden einfachen Test, um zu schauen ob die Anfragen durchgingen.

```java
@Override
public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
	res.setContentType("text/plain");
	PrintWriter out = res.getWriter();
	out.println("Hallo!");
}
```

Doch dies funktionierte nicht. Der Grund, das Servlet wurde von Spring nicht erkannt und daher auch nicht gestartet. Um die Erkennung zu aktivieren, braucht man die `@ServletComponentScan`-Annotation. Dadurch funktionierte mein kleiner Test von vorher.

```java
@SpringBootApplication
@ServletComponentScan //Scans for Servlets and starts them
public class SearcherApplication {

    public static void main(String[] args) {
        SpringApplication.run(SearcherApplication.class, args);
    }

}
```

Mit der Zeit erkannte ich, dass ich auf das Servlet per Spring zugreifen muss, um mehr Funktionalitäten zu ermöglichen. Um ein Servlet programm-technisch von Spring verwalten zu lassen, benutzt man ein `ServletRegistrationBean`. In diesem startet und konfiguriert man das Servlet auch.

```java
@Bean
@SuppressWarnings("unchecked")
public ServletRegistrationBean beanPrimeSearcher() {
    ServletRegistrationBean bean = new ServletRegistrationBean(
    	new PrimeSearcher());
    bean.addUrlMappings("/primes", "/primes/searcher");
    bean.setName("PrimeSearcher");
    bean.setLoadOnStartup(1);
    return bean;
}
```

Als nächstes beschäftigte ich mich mit der Ausgabe der Daten. Zur Ausgabe sollte eine HTML-Seite benutzt werden, diese muss aber ja mit den gefunden Primzahlen und Zeitstempeln ausgegeben werden. Dazu gibt es die Java-Template-Engine Thymeleaf, die dynamisch Objekt-Werte und Funktionen in eine spezielle HTML-Seite einfügt und diese dann als konforme HTML-Seite zurückgibt. 

Der Inhalt der HTML-Seite mit Thymeleaf für die Ausgabe sieht so aus:

```html
<body class="container">
    <div th:if="${terminatedFlag}">
        <h1>Prime Searcher - Findings</h1>
        <p>
            Started at <b><span th:text="${initDate}">Error?</span></b>. <br>
            The last prime discovered was <b><span th:text="${lastPrime}">Error?</span</b> 			   at <b><span th:text="${lastDate}">Error?</span></b>.
        </p>
    </div>
    <div th:unless="${terminatedFlag}">
        <h1>Prime Searcher has been stopped</h1>
    </div>
</body>
```

*Auszug aus searcher.html*

Thymeleaf unterstützt *if - else* Anweisungen, so können verschiedene Ausgaben gemacht werden, je nachdem welchen Zustand das Servlet gerade besitzt. Die Daten werden per ${variablen_name} dynamisch gefüllt.

Das schwierigste war das Implementieren von Thymeleaf in der `doGet`-Method des Servlets, wo die Ausgabe der Seiten geschieht. Während Spring-Controller Thymeleaf nativ unterstützen, sieht es beim einfachen Servlet anders aus. Man muss per Thymeleaf-Resolver einen String aus der konvertierten Seite erzeugen, der dann per einem `PrintWriter` ausgegeben wird und dann schlussendlich beim Client wieder als HTML-Seite angezeigt wird. Die Thymeleaf-Konversion sieht so aus:

```java
WebContext context = new WebContext(req, res, req.getServletContext());
        if (reqURL.equals("/primes")) {
            context.setVariable("recipient", "World");
            context.setVariable("url", "/primes/searcher");
            context.setVariable("terminatedFlag", searching);
            // Tells to process the Variables to the html-template via Thymeleaf
            String result = engine.process("/index.html", context);
            // Output result
            PrintWriter out = res.getWriter();
            if (result != null) {
                out.println(result);
            }
```

*Auszug aus `doGet` - PrimeSearcher*

Als nächstes widmete ich mich der automatischen Umleitung von `/primes` auf ``/primes/searcher`. Ich stieß auf ein Problem, denn ich gebe auch auf der `/primes`-URL auch eine Seite aus, somit kann ich kein einfaches redirect in der `doGet`-Methode per `res.sendRedirect("/primes/searcher")` implementieren, da man zu jeder Request nur eine Response zurückgeben kann, was in meinem Fall eine Seite und kein Redirect ist.

Ich löste das Problem, indem ich eine Art trojanisches Pferd aus meiner zurückgegeben Seite machte. Per JavaScript wird in dieser die Umleitung auf "/primes/searcher" realisiert.

```javascript
<script th:inline="javascript">
        if ([[${terminatedFlag}]]) {
            window.setTimeout(
                function () {
                    window.location.href = [[${url}]];
                }, 5000);
        }
</script>
```

*Auszug aus index.html*

Nachdem ich dies funktionierte widmete ich mich der Logik des Programmes. Ich erstellte in der `PrimeSearcher`-Klasse eine `run`-Methode, in der nach Primzahlen geforscht wird. Zur Bestimmung verwende ich folgenden Algorithmus.

```java
private boolean isPrime(long number) {
    // When number is dividable through 2 -> no prime
    if (number %2 == 0) return false;
    // Check other divisors
    for (int i = 3; i*i <=number; i+=2) {
        if(number %i == 0)
            return false;
    }
    return true;
}
```

Nach der Beendigung der Logik konnte die Ausgabe mit richtigen Werten stattfinden.

![Prime Searcher](img/output.png)

*Ausgabe der letzen Primzahl*

Der nächste Schritt war die Implementierung einer Shutdown Methode - je eine für die Applikation allgemein und eine für den internen Thread des Servlets. Die beiden Methoden werden durch Anfragen auf den `PrimeController` aufgerufen.

Das Schließen von der Applikation selbst ist keine große Schwierigkeit.

```java
@GetMapping("/shutdown")
public void shutdown() {
    SpringApplication.exit(ctx, () -> 0);
    System.exit(0);
}
```

Das Beenden des Servlets ist meines Wissens nach nicht wirklich machbar, man kann aber den internen Thread beenden, wodurch die Primzahl-Suche beendet wird, das URL-Mapping der Servlets funktioniert aber dennoch weiter.

```java
@GetMapping("/primes/shutdown")
    public String primesShutdown(Model model) {
    PrimeSearcher servlet = (PrimeSearcher) beanPrimeSearcher.getServlet();
    servlet.stop();
    model.addAttribute("terminatedFlag", false);
    return "index";
}
```

Das Beenden des internen Threads von `PrimeSearcher` erfolgt per `servlet.stop()`. Dabei wird nichts anderes gemacht, als das interne Flag einer while-Schleife auf *false* gesetzt. 

Ich probierte für das Beenden zusätzliche Funktionen zu verwenden wie `thread.interrupt` (verwendete anfangs nur einen Thread, keinen ExecutorService) oder `executor.shutdown` um sicherzustellen, dass der Thread beendet wurde. Doch das Aufrufen dieser Methode führte nur zu Fehlern beim Schließen der Anwendung, also entfernte ich diese.

Jetzt war die Anwendung grundsätzlich abgeschlossen, somit folgte das Testen.

---

Die grundsätzlichen Tests erwiesen sich als nicht sehr schwierig, es wird eine Abfrage an die REST-Schnittstellen gemacht, dadurch erhält man einen String mit der HTML-Seite. In dieser kann man schauen ob HTML-Elemente vorhanden sind.

```java
@Test
public void test1_IndexView() {
    assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/primes", 			String.class)).contains("Welcome to Prime Searcher!");
}
```

Bisschen schwieriger, war das Testen der letzten Werte. Ich musste eine Methode `syncServlet` erstellen, diese verlangsamt die Rate, in der neue Primzahlen gesucht werden. Erst dadurch kann man zeitlich die letzte Primzahl und die Webausgabe miteinander vergleichen, ohne dass zwischendurch eins von beiden sich verändert hat.

```java
public void syncServlet() throws Exception{
    // Change timeout in which the servlet searches for new primes
    servlet.setTimeout(2000);
    // Wait until new one is found, so that the new timeout will come in effect
    long last = servlet.getLastPrime();
    while (last == servlet.getLastPrime()) {
    	Thread.sleep(50);
    }
}
```

```java
@Test
public void test6_LastPrime() throws Exception {
    this.syncServlet();
    // Get last prime number and compare to Web-Output
    String lastPrime = String.valueOf(servlet.getLastPrime());
    assertThat(this.restTemplate.getForObject("http://localhost:" + port + 			 "/primes/searcher", String.class)).contains(lastPrime);
    // Restore old timeout
    servlet.setTimeout(100);
}
```

Das Schwierigste am Abstand war das Testen der Weiterleitung. Ein normaler Weiterleitungs-Test, falls man die Weiterleitung per Spring-Controller macht, sieht circa so aus:

```java
@Test
  public void testMyRedirect() throws Exception {
   mockMvc.perform(post("you/url/")
    .andExpect(status().isOk())
    .andExpect(redirectUrl("you/redirect")
}
```

Da meine Weiterleitung aber auf JavaScript setzt musste ich umdenken. Ich fand aber das tolle Framework *HtmlUnit*, dass diese schwierige Aufgabe ganz gut bewältigt.

*HtmlUnit* ist eine Art Browser-Simulator, ohne GUI und in Java gemacht. Mit diesen kann man Seiten besuchen und Aktionen aus-programmieren. Ich gebe einfach dem `WebClient` die Anweisung, dass er die Seite "/primes" besuchen soll, dann warte ich sechs Sekunden und hole mir seine aktuelle Seite, was hoffetnlich "primes/searchers" ist - die weitergeleitete Seite. 

```java
@Test
public void test3_IndexRedirect() throws Exception {
    // Create WebClient
    WebClient webClient = createWebClient();
    String originalURL = "http://localhost:" + port + "/primes";
    // Visit page to test redirection, save browser history count
    webClient.getPage(originalURL);
    int initHistory = webClient.getCurrentWindow().getHistory().getLength();
    // Wait 6 seconds, so that redirection will be invoked (through JavaScript
    // after 5 seconds)
    Thread.sleep(6000);
    // Get current browser history count and current url
    int laterHistory = webClient.getCurrentWindow().getHistory().getLength();
    String redURL = webClient.getCurrentWindow().getHistory().getUrl(laterHistory-1).toString();
    // Check if redirection has happened
    assertTrue(initHistory < laterHistory);
    assertEquals("http://localhost:" + port + "/primes/searcher", redURL);
}
```

## Quellen

* [Spring WebServlet "finden und starten"](https://stackoverflow.com/questions/27785935/spring-boot-does-not-honor-webservlet)
* [Java Servlet Programming](https://books.google.at/books?id=dsU4Lk-Gwk0C&pg=PA54&lpg=PA54&dq=Prime+searcher+in+java&source=bl&ots=pmFaQ4XrFR&sig=ACfU3U1h3AcAHySQ5Chqj9W0pTCFSApHLA&hl=de&sa=X&ved=2ahUKEwievu-2nsDhAhUID2MBHcorCzkQ6AEwAXoECAgQAQ#v=onepage&q=Prime%20searcher%20in%20java&f=false)
* [Redirects with Java](https://stackoverflow.com/questions/6175722/response-sendredirect-from-servlet-to-jsp-does-not-seem-to-work)
* [WebServlet HTML+Thymeleaf Ausgabe](https://github.com/nulab/thymeleaf-servlet-example/blob/master/src/main/java/jp/co/nulab/thymeleaf/servlet/ThymeleafServlet.java)
* [Thymeleaf Resolver Static](https://stackoverflow.com/questions/40164725/spring-thymeleaf-templateresolver-error)
* [Redirects with JavaScript](https://stackoverflow.com/questions/24700208/mvc-redirect-to-another-page-after-a-four-second-pause)
* [Prime Seacher Algorithmus](https://www.mkyong.com/java/how-to-determine-a-prime-number-in-java/)
* [Spring Shutdown](https://www.baeldung.com/spring-boot-shutdown)
* [Testing Spring](https://spring.io/guides/gs/testing-web/)
* [Testing HtmlUnit Framework](http://htmlunit.sourceforge.net/)
* [Testing HtmlUnit Redirect](https://stackoverflow.com/questions/42068052/how-to-handle-javascript-redirects-in-jsoup)
* [Testing Spring Redirect](https://stackoverflow.com/questions/29906356/in-my-junit-test-how-do-i-verify-a-spring-redirectview)